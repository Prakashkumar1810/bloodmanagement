from django.db import models

# Create your models here.

class Donor(models.Model):
    phone = models.CharField('Phone number',max_length=15,primary_key=True)
    name = models.CharField('Name',max_length=20)
    email = models.CharField('Email',max_length=30)
    password = models.CharField('Password',max_length=100)
    last_donation = models.DateField('Last Donation',auto_now_add=True)
    preferred_gap = models.IntegerField('Preferred gap',default=6)
    gender = models.CharField('Gender',max_length=10)
    blood_group = models.CharField('Blood group',max_length=10)
    birthday = models.DateField('Birthday',auto_now_add=True)
    place = models.CharField('Place',max_length=20)
    
    def __str__(self):
        return self.name

class Bloodbanks(models.Model):
    phone = models.CharField('Phone number',max_length=15,primary_key=True)
    name = models.CharField('Name',max_length=20)
    email = models.CharField('Email',max_length=30)
    place = models.CharField('Place',max_length=20)
    
    def __str__(self):
        return self.name

class Acceptor(models.Model):
    phone = models.CharField('Phone number',max_length=15,primary_key=True)
    name = models.CharField('Name',max_length=20)
    email = models.CharField('Email',max_length=30)
    password = models.CharField('Password',max_length=100)
    gender = models.CharField('Gender',max_length=10)
    blood_group = models.CharField('Blood group',max_length=10)
    birthday = models.DateField('Birthday',auto_now_add=True)
    place = models.CharField('Place',max_length=20)
    
    def __str__(self):
        return self.name

class Donations(models.Model):
    sname = models.CharField('Sname',max_length=30)
    rname = models.CharField('Rname',max_length=30)
    amount = models.CharField('Amount',max_length=30)
    
    def __str__(self):
        return self.sname